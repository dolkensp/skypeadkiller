unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, Vcl.Controls, Vcl.StdCtrls,
  System.Classes,  System.Variants, Vcl.Graphics,
    Vcl.Forms, Vcl.Dialogs, shellapi, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    resultLb: TLabel;
    lbl2: TLabel;
    chk1: TCheckBox;
    tmr1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure lbl2Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
  private
  public
    procedure KillAdFrom(mainWnd: string);
    procedure KillAdFromHwnd(conv: HWND; skype: HWND);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
function EnumWindowsCallback(hwnd: HWND; lParam: LPARAM): boolean; stdcall;
var buf: string[255];
 size: Integer;
 err: Cardinal;
begin
  result := true;
  size := GetClassNameA(hwnd, @buf[1], 255);
  if (size=0) then 
  begin
(*
    err := GetLastError();
    size := FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, nil, err, MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT), @buf[1], 255, nil);
    SetLength(buf, size);
    Form1.mmo1.Lines.Add(buf);
    *)
    exit;
  end;
  SetLength(buf, size);  
  if (buf = 'TConversationForm') then
  begin
    //Form1.mmo1.Lines.Add(buf);
    Form1.KillAdFromHwnd(hwnd, lParam);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  resultLb.Caption := '';
  KillAdFrom('tSkMainForm');
end;

procedure TForm1.KillAdFrom(mainWnd: String);
var skype: HWND;
begin
  skype := FindWindow(PWideChar(mainWnd), nil);

  if (skype<>0) then
  begin
    EnumChildWindows(skype, @EnumWindowsCallback, skype);
    EnumWindows(@EnumWindowsCallback, 0);
  end;
end;

procedure TForm1.KillAdFromHwnd(conv: HWND; skype: HWND);
var conversation, chatEntry, chat, browser,
  shell, shelldoc, ie, chatContent, contactProfile, callBar: HWND;
  res: Boolean;
  r1, r2, r3, r4, r5, r6: TRect;
  p: TPoint;
  dy: Integer;
begin
  resultLb.Caption := '';
  if (skype <> 0) then
    chat := FindWindowEx(skype, 0, 'TChatBanner', nil)
  else
    chat := FindWindowEx(conv, 0, 'TChatBanner', nil);
  if (chat=0) then
  begin
    resultLb.Caption := 'Skype found, but cant find ad(1)';
    exit;
  end;

  chatContent := FindWindowEx(conv, 0, 'TChatContentControl', nil);
  chatEntry := FindWindowEx(conv, 0, 'TChatEntryControl', nil);
  callBar := FindWindowEx(conv, 0, 'TNonLiveCallToolbar', nil);
  contactProfile := FindWindowEx(conv, 0, 'TContactProfile', nil);

  if (skype <> 0) then
  begin
    // In Default-mode hide ad and resize chat.

    GetWindowRect(chatEntry, r1); // TChatEntryControl
    GetWindowRect(conv, r2); // TConversationForm
    GetWindowRect(chatContent, r4); // TChatContentControl
    GetWindowRect(contactProfile, r5);
    GetWindowRect(callBar, r6);
    winapi.windows.GetClientRect(skype, r3);

    p := r2.TopLeft;
    winapi.windows.ScreenToClient(skype, p); //tskmainform

    SetWindowPos(chat, 0, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOZORDER);
    SetWindowPos(conv, 0, p.x, 0, r2.Width, r3.Height, SWP_NOZORDER);
    dy := r3.Height-r5.Height-r1.Height-r6.Height-3; // height
    SetWindowPos(chatContent, 0, 0, 0, r4.Width, dy, SWP_NOZORDER or SWP_NOMOVE);
  end
  else
  begin
    GetWindowRect(chat, r1); // TChatBanner
    GetWindowRect(chatEntry, r2); // TChatEntryControl
    GetWindowRect(chatContent, r4); // TConversationForm
    GetWindowRect(contactProfile, r5);
    winapi.windows.GetClientRect(conv, r3);

    SetWindowPos(chat, 0, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOZORDER);
    SetWindowPos(contactProfile, 0, 0, 0, 0, 0, SWP_NOSIZE or SWP_NOZORDER);
    SetWindowPos(callBar, 0, 0, r5.Height, 0, 0, SWP_NOSIZE or SWP_NOZORDER);
    SetWindowPos(chatContent, 0, 0, 0, r4.Width, r3.Height-r2.Height-10, SWP_NOZORDER);
    dy := r3.Height-r2.Height; // height change
  end;

    // kill ad.

    browser := FindWindowEx(chat, 0, 'TBrowserControl', nil);
    if (browser=0) then
    begin
      resultLb.Caption := 'Skype found, but cant find ad(2). Wait until it is shown';
      exit;
    end;
    shell := FindWindowEx(browser, 0, 'Shell Embedding', nil);
    if (shell=0) then
    begin
      resultLb.Caption := 'Skype found, but cant find ad(3)';
      exit;
    end;
    shelldoc := FindWindowEx(shell, 0, 'Shell DocObject View', nil);
    if (shelldoc=0) then
    begin
      resultLb.Caption := 'Skype found, but cant find ad(4)';
      exit;
    end;
    ie := FindWindowEx(shelldoc, 0, 'Internet Explorer_Server', nil);
    if (ie=0) then
    begin
      resultLb.Caption := 'Ads already removed';
      exit;
    end;

    if (not IsWindowVisible(ie)) then
    begin
      exit;
    end;

    SendMessage(ie, WM_CLOSE, 0, 0);
//    PostMessage(conv, WM_SYSCOMMAND, SC_MINIMIZE, 0);
//    PostMessage(conv, WM_SYSCOMMAND, SC_RESTORE, 0);
end;

procedure TForm1.lbl2Click(Sender: TObject);
begin
  ShellExecute(handle, 'open', 'http://blog.avangardo.com', nil, nil, SW_SHOWNORMAL);
end;

procedure TForm1.tmr1Timer(Sender: TObject);
begin
  if (not chk1.Checked) then exit;
  KillAdFrom('tSkMainForm');
end;

end.
